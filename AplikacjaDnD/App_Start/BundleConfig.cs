﻿using System.Web;
using System.Web.Optimization;

namespace AplikacjaDnD
{
    public class BundleConfig
    {
        // Aby uzyskać więcej informacji o grupowaniu, odwiedź stronę https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Użyj wersji deweloperskiej biblioteki Modernizr do nauki i opracowywania rozwiązań. Następnie, kiedy wszystko będzie
            // gotowe do produkcji, użyj narzędzia do kompilowania ze strony https://modernizr.com, aby wybrać wyłącznie potrzebne testy.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/materialize").Include(
                      "~/Scripts/materialize.js", "~/Scripts/home.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/materialize.css"));

            bundles.Add(new StyleBundle("~/Content/reset").Include(
                      "~/Content/ResetCSS.css"));

            bundles.Add(new StyleBundle("~/Content/home").Include(
                      "~/Content/Home.css"));

            bundles.Add(new StyleBundle("~/Content/gracz").Include(
                   "~/Content/Gracz.css"));

            bundles.Add(new StyleBundle("~/Content/kontogracz").Include(
                   "~/Content/KontoGracz.css"));

            bundles.Add(new StyleBundle("~/Content/buttoneffect").Include(
                   "~/Content/buttonsEffect.css"));
        }
    }
}
