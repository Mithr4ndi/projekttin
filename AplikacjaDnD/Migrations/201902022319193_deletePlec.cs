namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deletePlec : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Postaci", "Plec");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Postaci", "Plec", c => c.String(nullable: false));
        }
    }
}
