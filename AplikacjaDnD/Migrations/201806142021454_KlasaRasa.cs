namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KlasaRasa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Klasy",
                c => new
                    {
                        IdKlasa = c.Int(nullable: false, identity: true),
                        BonusKlasowy = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdKlasa);
            
            CreateTable(
                "dbo.Rasy",
                c => new
                    {
                        IdRasa = c.Int(nullable: false, identity: true),
                        Przywilej = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdRasa);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Rasy");
            DropTable("dbo.Klasy");
        }
    }
}
