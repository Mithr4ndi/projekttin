namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UniqueIndex : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.GraczPrzygody", new[] { "IdGracz" });
            DropIndex("dbo.GraczPrzygody", new[] { "IdPrzygoda" });
            CreateIndex("dbo.GraczPrzygody", new[] { "IdGracz", "IdPrzygoda" }, unique: true, name: "IX_FirstAndSecond");
        }
        
        public override void Down()
        {
            DropIndex("dbo.GraczPrzygody", "IX_FirstAndSecond");
            CreateIndex("dbo.GraczPrzygody", "IdPrzygoda");
            CreateIndex("dbo.GraczPrzygody", "IdGracz");
        }
    }
}
