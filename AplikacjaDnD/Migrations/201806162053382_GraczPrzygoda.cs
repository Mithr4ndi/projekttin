namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GraczPrzygoda : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GraczPrzygody",
                c => new
                    {
                        IdGraczPrzygoda = c.Int(nullable: false, identity: true),
                        IdGracz = c.Int(nullable: false),
                        IdPrzygoda = c.Int(nullable: false),
                        DataRozpoczecia = c.DateTime(nullable: false),
                        DataZakoczenia = c.DateTime(nullable: false),
                        Gracz_IdOsoba = c.Int(),
                    })
                .PrimaryKey(t => t.IdGraczPrzygoda)
                .ForeignKey("dbo.Osoby", t => t.Gracz_IdOsoba)
                .ForeignKey("dbo.Przygody", t => t.IdPrzygoda, cascadeDelete: true)
                .Index(t => t.IdPrzygoda)
                .Index(t => t.Gracz_IdOsoba);
            
            CreateTable(
                "dbo.Przygody",
                c => new
                    {
                        IdPrzygoda = c.Int(nullable: false, identity: true),
                        Tytul = c.String(nullable: false, maxLength: 255),
                        PoziomTrudnosci = c.Int(nullable: false),
                        LiczbaGraczy = c.Int(nullable: false),
                        LiczbaLokacji = c.Int(nullable: false),
                        Fabula = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.IdPrzygoda);
            
            AddColumn("dbo.Postaci", "IdGracz", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GraczPrzygody", "IdPrzygoda", "dbo.Przygody");
            DropForeignKey("dbo.GraczPrzygody", "Gracz_IdOsoba", "dbo.Osoby");
            DropIndex("dbo.GraczPrzygody", new[] { "Gracz_IdOsoba" });
            DropIndex("dbo.GraczPrzygody", new[] { "IdPrzygoda" });
            DropColumn("dbo.Postaci", "IdGracz");
            DropTable("dbo.Przygody");
            DropTable("dbo.GraczPrzygody");
        }
    }
}
