namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataZakon : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.GraczPrzygody", "DataZakonczenia", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.GraczPrzygody", "DataZakonczenia", c => c.DateTime(nullable: true));
        }
    }
}
