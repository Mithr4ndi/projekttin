namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LokacjaZadanie : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Lokacje",
                c => new
                    {
                        IdLokacja = c.Int(nullable: false, identity: true),
                        Krolestwo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdLokacja);
            
            CreateTable(
                "dbo.Zadania",
                c => new
                    {
                        IdZadanie = c.Int(nullable: false, identity: true),
                        Nagroda = c.String(nullable: false),
                        Lokacja_IdLokacja = c.Int(),
                    })
                .PrimaryKey(t => t.IdZadanie)
                .ForeignKey("dbo.Lokacje", t => t.Lokacja_IdLokacja)
                .Index(t => t.Lokacja_IdLokacja);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Zadania", "Lokacja_IdLokacja", "dbo.Lokacje");
            DropIndex("dbo.Zadania", new[] { "Lokacja_IdLokacja" });
            DropTable("dbo.Zadania");
            DropTable("dbo.Lokacje");
        }
    }
}
