namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Relacje_1wiele : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Zadania", "Lokacja_IdLokacja", "dbo.Lokacje");
            DropIndex("dbo.Zadania", new[] { "Lokacja_IdLokacja" });
            RenameColumn(table: "dbo.Zadania", name: "Lokacja_IdLokacja", newName: "IdLokacja");
            CreateTable(
                "dbo.Scenariusze",
                c => new
                    {
                        IdScenariusz = c.Int(nullable: false, identity: true),
                        Tytul = c.String(nullable: false, maxLength: 255),
                        Tresc = c.String(nullable: false, maxLength: 500),
                        IdMistrzGry = c.Int(nullable: false),
                        MistrzGry_IdOsoba = c.Int(),
                    })
                .PrimaryKey(t => t.IdScenariusz)
                .ForeignKey("dbo.Osoby", t => t.MistrzGry_IdOsoba)
                .Index(t => t.MistrzGry_IdOsoba);
            
            CreateTable(
                "dbo.Umiejetnosci",
                c => new
                    {
                        IdUmiejetnosc = c.Int(nullable: false, identity: true),
                        Moc = c.Int(nullable: false),
                        Efekt = c.Int(nullable: false),
                        IdKlasa = c.Int(nullable: false),
                        Nazwa = c.String(nullable: false),
                        Poziom = c.Int(nullable: false),
                        Opis = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdUmiejetnosc)
                .ForeignKey("dbo.Klasy", t => t.IdKlasa, cascadeDelete: true)
                .Index(t => t.IdKlasa);
            
            AddColumn("dbo.Przygody", "IdMistrzGry", c => c.Int(nullable: false));
            AddColumn("dbo.Przygody", "MistrzGry_IdOsoba", c => c.Int());
            AddColumn("dbo.Lokacje", "IdPrzygoda", c => c.Int(nullable: false));
            AddColumn("dbo.Potwory", "Lokacja_IdLokacja", c => c.Int());
            AlterColumn("dbo.Zadania", "IdLokacja", c => c.Int(nullable: false));
            AlterColumn("dbo.Potwory", "Typ", c => c.String(nullable: false));
            AlterColumn("dbo.Potwory", "Opis", c => c.String(nullable: false, maxLength: 500));
            CreateIndex("dbo.Przygody", "MistrzGry_IdOsoba");
            CreateIndex("dbo.Lokacje", "IdPrzygoda");
            CreateIndex("dbo.Potwory", "Lokacja_IdLokacja");
            CreateIndex("dbo.Zadania", "IdLokacja");
            AddForeignKey("dbo.Potwory", "Lokacja_IdLokacja", "dbo.Lokacje", "IdLokacja");
            AddForeignKey("dbo.Lokacje", "IdPrzygoda", "dbo.Przygody", "IdPrzygoda", cascadeDelete: true);
            AddForeignKey("dbo.Przygody", "MistrzGry_IdOsoba", "dbo.Osoby", "IdOsoba");
            AddForeignKey("dbo.Zadania", "IdLokacja", "dbo.Lokacje", "IdLokacja", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Zadania", "IdLokacja", "dbo.Lokacje");
            DropForeignKey("dbo.Umiejetnosci", "IdKlasa", "dbo.Klasy");
            DropForeignKey("dbo.Scenariusze", "MistrzGry_IdOsoba", "dbo.Osoby");
            DropForeignKey("dbo.Przygody", "MistrzGry_IdOsoba", "dbo.Osoby");
            DropForeignKey("dbo.Lokacje", "IdPrzygoda", "dbo.Przygody");
            DropForeignKey("dbo.Potwory", "Lokacja_IdLokacja", "dbo.Lokacje");
            DropIndex("dbo.Umiejetnosci", new[] { "IdKlasa" });
            DropIndex("dbo.Scenariusze", new[] { "MistrzGry_IdOsoba" });
            DropIndex("dbo.Zadania", new[] { "IdLokacja" });
            DropIndex("dbo.Potwory", new[] { "Lokacja_IdLokacja" });
            DropIndex("dbo.Lokacje", new[] { "IdPrzygoda" });
            DropIndex("dbo.Przygody", new[] { "MistrzGry_IdOsoba" });
            AlterColumn("dbo.Potwory", "Opis", c => c.String());
            AlterColumn("dbo.Potwory", "Typ", c => c.String());
            AlterColumn("dbo.Zadania", "IdLokacja", c => c.Int());
            DropColumn("dbo.Potwory", "Lokacja_IdLokacja");
            DropColumn("dbo.Lokacje", "IdPrzygoda");
            DropColumn("dbo.Przygody", "MistrzGry_IdOsoba");
            DropColumn("dbo.Przygody", "IdMistrzGry");
            DropTable("dbo.Umiejetnosci");
            DropTable("dbo.Scenariusze");
            RenameColumn(table: "dbo.Zadania", name: "IdLokacja", newName: "Lokacja_IdLokacja");
            CreateIndex("dbo.Zadania", "Lokacja_IdLokacja");
            AddForeignKey("dbo.Zadania", "Lokacja_IdLokacja", "dbo.Lokacje", "IdLokacja");
        }
    }
}
