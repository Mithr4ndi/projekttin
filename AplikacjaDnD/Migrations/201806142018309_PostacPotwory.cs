namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostacPotwory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Postaci",
                c => new
                    {
                        IdPostac = c.Int(nullable: false, identity: true),
                        Gracz_IdOsoba = c.Int(),
                    })
                .PrimaryKey(t => t.IdPostac)
                .ForeignKey("dbo.Osoby", t => t.Gracz_IdOsoba)
                .Index(t => t.Gracz_IdOsoba);
            
            CreateTable(
                "dbo.Potwory",
                c => new
                    {
                        IdPotwor = c.Int(nullable: false, identity: true),
                        Typ = c.String(),
                        Opis = c.String(),
                    })
                .PrimaryKey(t => t.IdPotwor);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Postaci", "Gracz_IdOsoba", "dbo.Osoby");
            DropIndex("dbo.Postaci", new[] { "Gracz_IdOsoba" });
            DropTable("dbo.Potwory");
            DropTable("dbo.Postaci");
        }
    }
}
