namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dfsg : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.GraczPrzygody", "DataRozpoczecia", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.GraczPrzygody", "DataRozpoczecia", c => c.DateTime(nullable: false));
        }
    }
}
