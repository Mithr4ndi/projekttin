namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ZmianaNazwyKolumny1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GraczPrzygody", "DataZakonczenia", c => c.DateTime(nullable: false));
            DropColumn("dbo.GraczPrzygody", "DataZakoczenia");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GraczPrzygody", "DataZakoczenia", c => c.DateTime(nullable: false));
            DropColumn("dbo.GraczPrzygody", "DataZakonczenia");
        }
    }
}
