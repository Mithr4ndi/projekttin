namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dodaniePolsaisMistrzGry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Osoby", "IsMistrzGry", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Osoby", "IsMistrzGry");
        }
    }
}
