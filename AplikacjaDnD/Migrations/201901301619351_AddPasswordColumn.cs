namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPasswordColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Osoby", "Haslo", c => c.String(nullable: false, maxLength: 18));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Osoby", "Haslo");
        }
    }
}
