namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodanieTypowNullowalnych : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Postaci", "PoziomZdrowia", c => c.Int());
            AlterColumn("dbo.Postaci", "Poziom", c => c.Int());
            AlterColumn("dbo.Postaci", "Doswiadczenie", c => c.Int());
            AlterColumn("dbo.Potwory", "Poziom", c => c.Int());
            AlterColumn("dbo.Potwory", "PoziomZdrowia", c => c.Int());
            AlterColumn("dbo.Potwory", "Doswiadczenie", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Potwory", "Doswiadczenie", c => c.Int(nullable: false));
            AlterColumn("dbo.Potwory", "PoziomZdrowia", c => c.Int(nullable: false));
            AlterColumn("dbo.Potwory", "Poziom", c => c.Int(nullable: false));
            AlterColumn("dbo.Postaci", "Doswiadczenie", c => c.Int(nullable: false));
            AlterColumn("dbo.Postaci", "Poziom", c => c.Int(nullable: false));
            AlterColumn("dbo.Postaci", "PoziomZdrowia", c => c.Int(nullable: false));
        }
    }
}
