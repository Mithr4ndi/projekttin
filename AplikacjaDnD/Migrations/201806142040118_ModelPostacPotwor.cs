namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelPostacPotwor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Postaci", "Nazwa", c => c.String(nullable: false));
            AddColumn("dbo.Postaci", "Poziom", c => c.Int(nullable: false));
            AddColumn("dbo.Postaci", "PoziomZdrowia", c => c.Int(nullable: false));
            AddColumn("dbo.Postaci", "PoziomMany", c => c.Int(nullable: true));
            AddColumn("dbo.Postaci", "Doswiadczenie", c => c.Int(nullable: false));
            AddColumn("dbo.Potwory", "Nazwa", c => c.String(nullable: false));
            AddColumn("dbo.Potwory", "Poziom", c => c.Int(nullable: false));
            AddColumn("dbo.Potwory", "PoziomZdrowia", c => c.Int(nullable: false));
            AddColumn("dbo.Potwory", "PoziomMany", c => c.Int(nullable: true));
            AddColumn("dbo.Potwory", "Doswiadczenie", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Potwory", "Doswiadczenie");
            DropColumn("dbo.Potwory", "PoziomMany");
            DropColumn("dbo.Potwory", "PoziomZdrowia");
            DropColumn("dbo.Potwory", "Poziom");
            DropColumn("dbo.Potwory", "Nazwa");
            DropColumn("dbo.Postaci", "Doswiadczenie");
            DropColumn("dbo.Postaci", "PoziomMany");
            DropColumn("dbo.Postaci", "PoziomZdrowia");
            DropColumn("dbo.Postaci", "Poziom");
            DropColumn("dbo.Postaci", "Nazwa");
        }
    }
}
