namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostacPrzedmiotyRelacja : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bronie", "IdPostac", c => c.Int(nullable: false));
            AddColumn("dbo.Pancerz", "IdPostac", c => c.Int(nullable: false));
            CreateIndex("dbo.Bronie", "IdPostac");
            CreateIndex("dbo.Pancerz", "IdPostac");
            AddForeignKey("dbo.Bronie", "IdPostac", "dbo.Postaci", "IdPostac", cascadeDelete: true);
            AddForeignKey("dbo.Pancerz", "IdPostac", "dbo.Postaci", "IdPostac", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pancerz", "IdPostac", "dbo.Postaci");
            DropForeignKey("dbo.Bronie", "IdPostac", "dbo.Postaci");
            DropIndex("dbo.Pancerz", new[] { "IdPostac" });
            DropIndex("dbo.Bronie", new[] { "IdPostac" });
            DropColumn("dbo.Pancerz", "IdPostac");
            DropColumn("dbo.Bronie", "IdPostac");
        }
    }
}
