namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelKlasaRasa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Klasy", "Nazwa", c => c.String(nullable: false));
            AddColumn("dbo.Klasy", "Historia", c => c.String(nullable: false));
            AddColumn("dbo.Rasy", "Nazwa", c => c.String(nullable: false));
            AddColumn("dbo.Rasy", "Historia", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rasy", "Historia");
            DropColumn("dbo.Rasy", "Nazwa");
            DropColumn("dbo.Klasy", "Historia");
            DropColumn("dbo.Klasy", "Nazwa");
        }
    }
}
