namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodaniePolaStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Przygody", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Przygody", "Status");
        }
    }
}
