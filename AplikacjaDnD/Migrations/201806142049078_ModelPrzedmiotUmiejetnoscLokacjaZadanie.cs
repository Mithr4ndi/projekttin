namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelPrzedmiotUmiejetnoscLokacjaZadanie : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bronie", "Nazwa", c => c.String(nullable: false));
            AddColumn("dbo.Bronie", "Poziom", c => c.Int(nullable: false));
            AddColumn("dbo.Bronie", "Opis", c => c.String(nullable: false));
            AddColumn("dbo.Lokacje", "Nazwa", c => c.String(nullable: false));
            AddColumn("dbo.Lokacje", "Poziom", c => c.Int(nullable: false));
            AddColumn("dbo.Lokacje", "Opis", c => c.String(nullable: false));
            AddColumn("dbo.Zadania", "Nazwa", c => c.String(nullable: false));
            AddColumn("dbo.Zadania", "Poziom", c => c.Int(nullable: false));
            AddColumn("dbo.Zadania", "Opis", c => c.String(nullable: false));
            AddColumn("dbo.Pancerz", "Nazwa", c => c.String(nullable: false));
            AddColumn("dbo.Pancerz", "Poziom", c => c.Int(nullable: false));
            AddColumn("dbo.Pancerz", "Opis", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pancerz", "Opis");
            DropColumn("dbo.Pancerz", "Poziom");
            DropColumn("dbo.Pancerz", "Nazwa");
            DropColumn("dbo.Zadania", "Opis");
            DropColumn("dbo.Zadania", "Poziom");
            DropColumn("dbo.Zadania", "Nazwa");
            DropColumn("dbo.Lokacje", "Opis");
            DropColumn("dbo.Lokacje", "Poziom");
            DropColumn("dbo.Lokacje", "Nazwa");
            DropColumn("dbo.Bronie", "Opis");
            DropColumn("dbo.Bronie", "Poziom");
            DropColumn("dbo.Bronie", "Nazwa");
        }
    }
}
