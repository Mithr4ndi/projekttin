namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BronPancerz : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bronie",
                c => new
                    {
                        IdBron = c.Int(nullable: false, identity: true),
                        Rzadkosc = c.Int(nullable: false),
                        czyMagiczny = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdBron);
            
            CreateTable(
                "dbo.Pancerz",
                c => new
                    {
                        IdPancerz = c.Int(nullable: false, identity: true),
                        Rzadkosc = c.Int(nullable: false),
                        czyMagiczny = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdPancerz);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pancerz");
            DropTable("dbo.Bronie");
        }
    }
}
