namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ZmianaTypuPolaStatus : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Przygody", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Przygody", "Status", c => c.Boolean(nullable: false));
        }
    }
}
