namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodanieKolumnyPlec : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Postaci", "Plec", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Postaci", "Plec");
        }
    }
}
