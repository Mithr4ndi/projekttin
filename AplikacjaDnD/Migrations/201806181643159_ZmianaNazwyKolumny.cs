namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ZmianaNazwyKolumny : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Osoby", name: "Nick", newName: "Nick gracza");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Osoby", name: "Nick gracza", newName: "Nick");
        }
    }
}
