namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PoprawkaKolumny : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Osoby", name: "Nick gracza", newName: "Nick");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Osoby", name: "Nick", newName: "Nick gracza");
        }
    }
}
