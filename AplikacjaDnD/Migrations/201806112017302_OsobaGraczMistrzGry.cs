namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GraczMistrzGry : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Osoby",
                c => new
                    {
                        IdOsoba = c.Int(nullable: false, identity: true),
                        Imie = c.String(nullable: false, maxLength: 100),
                        Nazwisko = c.String(nullable: false, maxLength: 100),
                        Nick = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 100),
                        DataUrodzenia = c.DateTime(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.IdOsoba);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Osoby");
        }
    }
}
