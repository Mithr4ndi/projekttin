namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OstatnieSzlify : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Postaci", "Gracz_IdOsoba", "dbo.Osoby");
            DropForeignKey("dbo.GraczPrzygody", "Gracz_IdOsoba", "dbo.Osoby");
            DropForeignKey("dbo.Przygody", "MistrzGry_IdOsoba", "dbo.Osoby");
            DropForeignKey("dbo.Scenariusze", "MistrzGry_IdOsoba", "dbo.Osoby");
            DropIndex("dbo.Postaci", new[] { "Gracz_IdOsoba" });
            DropIndex("dbo.GraczPrzygody", new[] { "Gracz_IdOsoba" });
            DropIndex("dbo.Przygody", new[] { "MistrzGry_IdOsoba" });
            DropIndex("dbo.Scenariusze", new[] { "MistrzGry_IdOsoba" });
            DropColumn("dbo.Postaci", "IdGracz");
            DropColumn("dbo.GraczPrzygody", "IdGracz");
            DropColumn("dbo.Przygody", "IdMistrzGry");
            DropColumn("dbo.Scenariusze", "IdMistrzGry");
            RenameColumn(table: "dbo.Postaci", name: "Gracz_IdOsoba", newName: "IdGracz");
            RenameColumn(table: "dbo.GraczPrzygody", name: "Gracz_IdOsoba", newName: "IdGracz");
            RenameColumn(table: "dbo.Przygody", name: "MistrzGry_IdOsoba", newName: "IdMistrzGry");
            RenameColumn(table: "dbo.Scenariusze", name: "MistrzGry_IdOsoba", newName: "IdMistrzGry");
            AddColumn("dbo.Bronie", "Rodzaj", c => c.Int(nullable: false));
            AddColumn("dbo.Bronie", "Obrazenia", c => c.Int(nullable: false));
            AddColumn("dbo.Bronie", "Zasieg", c => c.Int());
            AddColumn("dbo.Bronie", "Predkosc", c => c.Int(nullable: false));
            AddColumn("dbo.Bronie", "Bonus", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.Postaci", "IdKlasa", c => c.Int(nullable: false));
            AddColumn("dbo.Postaci", "IdRasa", c => c.Int(nullable: false));
            AddColumn("dbo.Pancerz", "Rodzaj", c => c.Int(nullable: false));
            AddColumn("dbo.Pancerz", "Typ", c => c.Int(nullable: false));
            AddColumn("dbo.Pancerz", "PunktyPancerza", c => c.Int(nullable: false));
            AddColumn("dbo.Pancerz", "Wytrzymalosc", c => c.Int(nullable: false));
            AlterColumn("dbo.Bronie", "Nazwa", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Postaci", "Nazwa", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Postaci", "IdGracz", c => c.Int(nullable: false));
            AlterColumn("dbo.Osoby", "Nick", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.GraczPrzygody", "IdGracz", c => c.Int(nullable: false));
            AlterColumn("dbo.Przygody", "Tytul", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Przygody", "IdMistrzGry", c => c.Int(nullable: false));
            AlterColumn("dbo.Lokacje", "Nazwa", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Potwory", "Nazwa", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Zadania", "Nazwa", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Scenariusze", "IdMistrzGry", c => c.Int(nullable: false));
            AlterColumn("dbo.Pancerz", "Nazwa", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Klasy", "Nazwa", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Umiejetnosci", "Nazwa", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Rasy", "Nazwa", c => c.String(nullable: false, maxLength: 20));
            CreateIndex("dbo.Bronie", "Nazwa", unique: true);
            CreateIndex("dbo.Postaci", "IdGracz");
            CreateIndex("dbo.Postaci", "IdKlasa");
            CreateIndex("dbo.Postaci", "IdRasa");
            CreateIndex("dbo.Postaci", "Nazwa", unique: true);
            CreateIndex("dbo.Osoby", "Nick", unique: true);
            CreateIndex("dbo.GraczPrzygody", "IdGracz");
            CreateIndex("dbo.Przygody", "IdMistrzGry");
            CreateIndex("dbo.Lokacje", "Nazwa", unique: true);
            CreateIndex("dbo.Potwory", "Nazwa", unique: true);
            CreateIndex("dbo.Zadania", "Nazwa", unique: true);
            CreateIndex("dbo.Scenariusze", "IdMistrzGry");
            CreateIndex("dbo.Klasy", "Nazwa", unique: true);
            CreateIndex("dbo.Umiejetnosci", "Nazwa", unique: true);
            CreateIndex("dbo.Pancerz", "Nazwa", unique: true);
            CreateIndex("dbo.Rasy", "Nazwa", unique: true);
            AddForeignKey("dbo.Postaci", "IdKlasa", "dbo.Klasy", "IdKlasa", cascadeDelete: true);
            AddForeignKey("dbo.Postaci", "IdRasa", "dbo.Rasy", "IdRasa", cascadeDelete: true);
            AddForeignKey("dbo.Postaci", "IdGracz", "dbo.Osoby", "IdOsoba", cascadeDelete: true);
            AddForeignKey("dbo.GraczPrzygody", "IdGracz", "dbo.Osoby", "IdOsoba", cascadeDelete: true);
            AddForeignKey("dbo.Przygody", "IdMistrzGry", "dbo.Osoby", "IdOsoba", cascadeDelete: false);
            AddForeignKey("dbo.Scenariusze", "IdMistrzGry", "dbo.Osoby", "IdOsoba", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Scenariusze", "IdMistrzGry", "dbo.Osoby");
            DropForeignKey("dbo.Przygody", "IdMistrzGry", "dbo.Osoby");
            DropForeignKey("dbo.GraczPrzygody", "IdGracz", "dbo.Osoby");
            DropForeignKey("dbo.Postaci", "IdGracz", "dbo.Osoby");
            DropForeignKey("dbo.Postaci", "IdRasa", "dbo.Rasy");
            DropForeignKey("dbo.Postaci", "IdKlasa", "dbo.Klasy");
            DropIndex("dbo.Rasy", new[] { "Nazwa" });
            DropIndex("dbo.Pancerz", new[] { "Nazwa" });
            DropIndex("dbo.Umiejetnosci", new[] { "Nazwa" });
            DropIndex("dbo.Klasy", new[] { "Nazwa" });
            DropIndex("dbo.Scenariusze", new[] { "IdMistrzGry" });
            DropIndex("dbo.Zadania", new[] { "Nazwa" });
            DropIndex("dbo.Potwory", new[] { "Nazwa" });
            DropIndex("dbo.Lokacje", new[] { "Nazwa" });
            DropIndex("dbo.Przygody", new[] { "IdMistrzGry" });
            DropIndex("dbo.GraczPrzygody", new[] { "IdGracz" });
            DropIndex("dbo.Osoby", new[] { "Nick" });
            DropIndex("dbo.Postaci", new[] { "Nazwa" });
            DropIndex("dbo.Postaci", new[] { "IdRasa" });
            DropIndex("dbo.Postaci", new[] { "IdKlasa" });
            DropIndex("dbo.Postaci", new[] { "IdGracz" });
            DropIndex("dbo.Bronie", new[] { "Nazwa" });
            AlterColumn("dbo.Rasy", "Nazwa", c => c.String(nullable: false));
            AlterColumn("dbo.Umiejetnosci", "Nazwa", c => c.String(nullable: false));
            AlterColumn("dbo.Klasy", "Nazwa", c => c.String(nullable: false));
            AlterColumn("dbo.Pancerz", "Nazwa", c => c.String(nullable: false));
            AlterColumn("dbo.Scenariusze", "IdMistrzGry", c => c.Int());
            AlterColumn("dbo.Zadania", "Nazwa", c => c.String(nullable: false));
            AlterColumn("dbo.Potwory", "Nazwa", c => c.String(nullable: false));
            AlterColumn("dbo.Lokacje", "Nazwa", c => c.String(nullable: false));
            AlterColumn("dbo.Przygody", "IdMistrzGry", c => c.Int());
            AlterColumn("dbo.Przygody", "Tytul", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.GraczPrzygody", "IdGracz", c => c.Int());
            AlterColumn("dbo.Osoby", "Nick", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Postaci", "IdGracz", c => c.Int());
            AlterColumn("dbo.Postaci", "Nazwa", c => c.String(nullable: false));
            AlterColumn("dbo.Bronie", "Nazwa", c => c.String(nullable: false));
            DropColumn("dbo.Pancerz", "Wytrzymalosc");
            DropColumn("dbo.Pancerz", "PunktyPancerza");
            DropColumn("dbo.Pancerz", "Typ");
            DropColumn("dbo.Pancerz", "Rodzaj");
            DropColumn("dbo.Postaci", "IdRasa");
            DropColumn("dbo.Postaci", "IdKlasa");
            DropColumn("dbo.Bronie", "Bonus");
            DropColumn("dbo.Bronie", "Predkosc");
            DropColumn("dbo.Bronie", "Zasieg");
            DropColumn("dbo.Bronie", "Obrazenia");
            DropColumn("dbo.Bronie", "Rodzaj");
            RenameColumn(table: "dbo.Scenariusze", name: "IdMistrzGry", newName: "MistrzGry_IdOsoba");
            RenameColumn(table: "dbo.Przygody", name: "IdMistrzGry", newName: "MistrzGry_IdOsoba");
            RenameColumn(table: "dbo.GraczPrzygody", name: "IdGracz", newName: "Gracz_IdOsoba");
            RenameColumn(table: "dbo.Postaci", name: "IdGracz", newName: "Gracz_IdOsoba");
            AddColumn("dbo.Scenariusze", "IdMistrzGry", c => c.Int(nullable: false));
            AddColumn("dbo.Przygody", "IdMistrzGry", c => c.Int(nullable: false));
            AddColumn("dbo.GraczPrzygody", "IdGracz", c => c.Int(nullable: false));
            AddColumn("dbo.Postaci", "IdGracz", c => c.Int(nullable: false));
            CreateIndex("dbo.Scenariusze", "MistrzGry_IdOsoba");
            CreateIndex("dbo.Przygody", "MistrzGry_IdOsoba");
            CreateIndex("dbo.GraczPrzygody", "Gracz_IdOsoba");
            CreateIndex("dbo.Postaci", "Gracz_IdOsoba");
            AddForeignKey("dbo.Scenariusze", "MistrzGry_IdOsoba", "dbo.Osoby", "IdOsoba");
            AddForeignKey("dbo.Przygody", "MistrzGry_IdOsoba", "dbo.Osoby", "IdOsoba");
            AddForeignKey("dbo.GraczPrzygody", "Gracz_IdOsoba", "dbo.Osoby", "IdOsoba");
            AddForeignKey("dbo.Postaci", "Gracz_IdOsoba", "dbo.Osoby", "IdOsoba");
        }
    }
}
