namespace AplikacjaDnD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmailUnique : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Osoby", "Email", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Osoby", new[] { "Email" });
        }
    }
}
