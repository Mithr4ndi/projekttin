namespace AplikacjaDnD.Migrations
{
    using AplikacjaDnD.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AplikacjaDnD.Models.DbDnD>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AplikacjaDnD.Models.DbDnD context)
        {
          /*  context.Gracze.AddOrUpdate(
                 g => g.Nick,
         new Gracz
         {
             IdOsoba = 1,
             Imie = "Jan",
             Nazwisko = "Kowalski",
             Nick = "Nexo",
             Email = "nexo@gmail.com",
             DataUrodzenia = new DateTime(1995, 12, 14)
         });

            context.SaveChanges();

            context.MistrzowieGry.AddOrUpdate(
                mg => mg.Nick,
           new MistrzGry
           {
               IdOsoba = 2,
               Imie = "Waldemar",
               Nazwisko = "Nowak",
               Nick = "Domestos",
               Email = "nowak@gmail.com",
               DataUrodzenia = new DateTime(1990, 10, 11)
           }
           );


           context.SaveChanges();

            context.Rasy.AddOrUpdate(
                r => r.Nazwa,
            new Rasa
            {
                Nazwa = "Nocny Elf",
                Historia = "Kaldorei to staro�ytna i samotnicza rasa, narodzona podczas przebudzenia si� �wiata. Kaldorei s� rozs�dne, ale zabobonne, co czasem prowadzi do paradoks�w. S� to istoty przepe�nione duchow� pasj� i pragn�ce rado�ci. " +
                "Jeszcze niedawno by�y nie�miertelne. Nocne elfy s� honorowe, co czasem staje si� ich wad�. Wci�� nie ufaj� pomniejszym rasom �wiata, kt�re s� nierozs�dne i �yj� zbyt kr�tko, by naprawi� b��dy " +
                "swojej przesz�o�ci. Wiele nocnych elf�w uwa�a dbanie o pok�j i r�wnowag� na �wiecie jako sw�j obowi�zek.To sprawia, �e wiele m�odych ras im nie ufa, nie tylko z powodu ich pot�nej magii, ale ich inno�ci i przesadnej sprawiedliwo�ci.",
                Przywilej = "Jedno�� z cieniem - Skrywanie si� w ciemno�ciach i oczekiwanie na odpowiedni moment do ataku jest drug� natur� nocnych elf�w."
            },

            new Rasa
            {
                Nazwa = "Worgen",
                Historia =" Worgeni to rasa dzikich ludzi - wilk�w, kt�rych samo imi� wzbudza strach.Kr��y wiele teorii na temat narodzin worgen�w, jednak same pocz�tki rasy s� owiane tajemnic�. " +
                "Zapiski historyczne wskazuj�,�e worgeni �yli przez pewien czas w Kalimdorze. Ostatnie badania wskazuj�, �e narodziny worgen�w mog� wi�za� si� z nocnymi elfami i tajemnym zakonem druid�w z dalekiej przesz�o�ci Kalimdoru.",
                Przywilej = "Dwa oblicza - Worgeni mog� przeskakiwa� pomi�dzy form� cz�owieka i worgena na �yczenie."
            },

            new Rasa
            {
                Nazwa = "Draenei",
                Historia = "Draenei to nieska�ona grupa eredar�w, kt�rzy uciekli ze swojej ojczystej ziemi - Argus. Ich baz� jest wyspa Lazurowej Mg�y, po�o�ona nieopodal zachodniego wybrze�a Kalimdoru." +
                "Kultura draenei opiera si� na dw�ch rzeczach: �wi�tej �wiat�o�ci Stworzenia i magii. Pierwsza z nich jest efektem ich unikalnych relacji z naaru, natomiast druga jest drog�, kt�r� eredarzy pod��aj� od zarania dziej�w.",
                Przywilej = "Dar Naaru - Draenei s� obdarzeni moc� leczenia, kt�ra mo�e odnowi� punkty �ycia ich, oraz sprzymierze�c�w."
            },

            new Rasa
            {
                Nazwa = "Troll",
                Historia = "Dzikie trolle Azerothu s� nies�awne z powodu ich brutalno�ci, mrocznego mistycyzmu i zaciek�ej nienawi�ci wobec innych ras. Z jednym wyj�tkiem, kt�rym jest plemi� Darkspear i jego przebieg�y lider, Vol�jin. Naznaczone histori� uleg�o�ci i wyp�dzenia, to dumne plemi� by�o na granicy wymarcia, gdy w�dz Thrall i jego pot�ne si�y Hordy zostali przygnani przez sztorm do odleg�ej wyspy-domu tych trolli na Po�udniowych Morzach.",
                Przywilej = "Ruchy Voodoo - Dzi�ki swej niebywa�ej gibko�ci, trolle s� w mniejszym stopniu wra�liwe na efekty zwi�zane z os�abianiem zdolno�ci poruszania si�, ni� inne rasy."
            }
                );

            context.Klasy.AddOrUpdate(
                k => k.Nazwa,

                new Klasa
                {
                    Nazwa = "Rycerz �mierci",
                    Historia = "Kiedy kontrola Kr�la Lisza nad jego rycerzami �mierci zosta�a prze�amana, jego niegdysiejsi czempioni zacz�li szuka� zemsty za okropie�stwa jakich dokonali pod jego dow�dztwem. Gdy j� wygrali, rycerze �mierci pozostali bez sprawy i bez domu. Jeden za drugim przenikali na ziemie �ywych w poszukiwaniu nowego celu.",
                    BonusKlasowy = "Przekuwanie run - Rycerze �mierci s� osobi�cie zwi�zani ze swymi ostrzami i mog� wkuwa� w nie runy w celu zwi�kszenia ich mocy."

                },

                new Klasa
                {
                    Nazwa = "Kap�an",
                    Historia = "Kap�ani s� oddani duchowo�ci i wyra�aj� sw� niezachwian� wiar� s�u��c ludziom. Na milenia pozostawili mury swych �wi�ty� i wygody ich kaplic, aby wspiera� swych sojusznik�w na rozdartych wojn� ziemiach. W �rodku okropnego konfliktu, �aden bohater nie kwestionuje warto�ci kap�a�skich �wi�ce�.",
                    BonusKlasowy = "S�owa mocy - Wzywaj�c mistyczn� tarcz� i �wi�te umocnienie, kap�an mo�e zapobiec nadchodz�cym obra�eniom i ulepszy� obron� swoj� i swoich sojusznik�w"
                },

                new Klasa
                {
                    Nazwa = "Lotrzyk",
                    Historia = "Dla �otrzyk�w, jedynym kodeksem jest kontrakt, a ich honor, jest kupowany za z�oto. Nieskr�powani sumieniem najemnicy ci polegaj� na brutalnych i efektywnych taktykach. Zab�jczy asasyni i mistrzowie skradania si�, podchodz� do swych cel�w od ty�u, przeszywaj� witalny organ i rozp�ywaj� si� w cieniu, zanim ich ofiara upadnie na ziemie.",
                    BonusKlasowy = "Skradanie si� - �otrzykowie skradaj� si� po polu bitwy, ukrywaj�c si� przed przeciwnikami i wyprowadzaj�c ataki z zaskoczenia na nieostro�nych, gdy nadarzy si� okazja."
                },

                new Klasa
                {
                    Nazwa = "Mag",
                    Historia = "M�odzi adepci obdarzeni ponadprzeci�tnym intelektem oraz niezachwian� dyscyplin� mog� pod��y� �cie�k� maga. Tajemna magia dost�pna dla mag�w jest zarazem wspania�a jak i niebezpieczna, przez co wyjawiana jest jedynie najbardziej oddanym praktykuj�cym. ",
                    BonusKlasowy = "Teleportacja - Magowie posiedli zdolno�� transportowania siebie oraz swoich sojusznik�w pomi�dzy miastami. Mog� tak�e przyzywa� uzupe�niaj�cy si�y pokarm i wod�"
                }
                
                );

            context.SaveChanges();


            context.Przygody.AddOrUpdate(
                k => k.IdPrzygoda,
             new Przygoda
             {
                 IdPrzygoda = 1,
                 Tytul = "Bitwa pod Angratharem",
                 PoziomTrudnosci = PoziomTrudnosci.Normalny,
                 LiczbaGraczy = 5,
                 LiczbaLokacji = 3,
                 Fabula = "Dawno temu w odleglej krainie... ",
                 IdMistrzGry = 2
                 

             },

            new Przygoda
            {
                IdPrzygoda = 2,
                Tytul = "Upadek Kr�la Licha",
                PoziomTrudnosci = PoziomTrudnosci.Trudny,
                LiczbaGraczy = 5,
                LiczbaLokacji = 4,
                Fabula = "Kiedy dotarli juz do lodowej cytadeli...",
                IdMistrzGry = 2

            },

            new Przygoda
            {
                IdPrzygoda = 3,
                Tytul = "Ucieczka z Durnholde",
                PoziomTrudnosci = PoziomTrudnosci.Normalny,
                LiczbaGraczy = 3,
                LiczbaLokacji = 2,
                Fabula = "Uciekali pod os�on� nocy...",
                IdMistrzGry = 2,
                Status = Status.Rozpoczeta

            },

            new Przygoda
            {
                IdPrzygoda = 4,
                Tytul = "Bitwa na Mount Hyjal",
                PoziomTrudnosci = PoziomTrudnosci.Trudny,
                LiczbaGraczy = 5,
                LiczbaLokacji = 4,
                Fabula = "Kiedy dotarli juz do lodowej cytadeli...",
                IdMistrzGry = 2,
                Status = Status.Zakonczona
            }
            );

            context.SaveChanges();

            context.GraczPrzygody.AddOrUpdate(
               g => g.IdGraczPrzygoda,
            new Models.GraczPrzygoda
            {
                IdGraczPrzygoda = 1,
                IdGracz = 1,
                IdPrzygoda = 3

            },

            new Models.GraczPrzygoda
            {
                IdGraczPrzygoda = 1,
                IdGracz = 1,
                IdPrzygoda = 4,
                DataZakonczenia = DateTime.Now
            }
               ); */

            context.SaveChanges();

        }
    }
}
