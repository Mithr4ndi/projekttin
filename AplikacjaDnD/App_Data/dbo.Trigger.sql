﻿CREATE TRIGGER [Trigger]
	ON [dbo].[Osoby]
	FOR UPDATE
	AS
	DECLARE
	@IsMistrzGry bit
	SELECT @IsMistrzGry = IsMistrzGry FROM INSERTED;
	BEGIN
		IF(@IsMistrzGry = 1)
		BEGIN
		UPDATE Osoby SET Discriminator = 'MistrzGry';
		END
	END
