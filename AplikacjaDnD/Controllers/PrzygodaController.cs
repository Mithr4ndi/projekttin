﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AplikacjaDnD.Models;

namespace AplikacjaDnD.Controllers
{
    public class PrzygodaController : Controller
    {
        private DbDnD db = new DbDnD();

        // GET: Przygoda
        public ActionResult Index()
        {
            var przygody = db.Przygody.Include(p => p.MistrzGry);
            return View(przygody.ToList());
        }

        // GET: Przygoda/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Przygoda przygoda = db.Przygody.Find(id);
            if (przygoda == null)
            {
                return HttpNotFound();
            }
            return View(przygoda);
        }

        // GET: Przygoda/Create
        public ActionResult Create()
        {
            ViewBag.IdMistrzGry = new SelectList(db.Osoby, "IdOsoba", "Imie");
            return View();
        }

        // POST: Przygoda/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPrzygoda,Tytul,PoziomTrudnosci,LiczbaGraczy,LiczbaLokacji,Fabula,IdMistrzGry,Status")] Przygoda przygoda)
        {
            if (ModelState.IsValid)
            {
                db.Przygody.Add(przygoda);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdMistrzGry = new SelectList(db.Osoby, "IdOsoba", "Imie", przygoda.IdMistrzGry);
            return View(przygoda);
        }

        // GET: Przygoda/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Przygoda przygoda = db.Przygody.Find(id);
            if (przygoda == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdMistrzGry = new SelectList(db.Osoby, "IdOsoba", "Imie", przygoda.IdMistrzGry);
            return View(przygoda);
        }

        // POST: Przygoda/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPrzygoda,Tytul,PoziomTrudnosci,LiczbaGraczy,LiczbaLokacji,Fabula,IdMistrzGry,Status")] Przygoda przygoda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(przygoda).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdMistrzGry = new SelectList(db.Osoby, "IdOsoba", "Imie", przygoda.IdMistrzGry);
            return View(przygoda);
        }

        // GET: Przygoda/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Przygoda przygoda = db.Przygody.Find(id);
            if (przygoda == null)
            {
                return HttpNotFound();
            }
            return View(przygoda);
        }

        // POST: Przygoda/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Przygoda przygoda = db.Przygody.Find(id);
            db.Przygody.Remove(przygoda);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DolaczDoPrzygody()
        {
            var przygody = db.Przygody.Include(p => p.MistrzGry).Where(p=>p.Status == Status.Brak);
            return View(przygody.ToList());
        }

        public ActionResult Dolacz(int? idPrzygoda)
        {
            if(idPrzygoda == null)
            {
                return View("DolaczDoPrzygody");
            }

            Przygoda przygoda = db.Przygody.Find(idPrzygoda);
            var idGracz = (int)Session["userId"];

            //----LiczbaGraczy-------
            int liczbaGraczy = PodajLiczbeGraczy(idPrzygoda);

            if (liczbaGraczy >= przygoda.LiczbaGraczy)
            {
                return RedirectToAction("DolaczDoPrzygody");
            }

            //----CzyIstnieje---------
           int graczPrzygoda = SprawdzCzyIstnieje(idGracz, idPrzygoda);
                
            if(graczPrzygoda > 0)
            {
                return View("ErrorGraczPrzygody");
            }

            //----DodanieDoBazy-------
            DodajGraczPrzygoda(idGracz, przygoda);

            //----ZmianaStatusu-------

            if (liczbaGraczy + 1 >= przygoda.LiczbaGraczy)
            {
                ZmianaStatusuPrzygody(liczbaGraczy, przygoda);
            }

            return View( "Koniec");
        }

        public ActionResult Kontynuuj()
        {
            return View("Kontynuuj");
        }
        private int SprawdzCzyIstnieje(int idGracz, int? idPrzygoda)
        {
            var graczPrzygoda = (int)(from gp in db.GraczPrzygody
                                      where gp.IdPrzygoda == idPrzygoda && gp.IdGracz == idGracz
                                      select gp).Count();
                return graczPrzygoda;
        }

        private int PodajLiczbeGraczy(int? idPrzygoda)
        {
            var liczbaGraczy = (int)(from gp in db.GraczPrzygody
                                where gp.IdPrzygoda == idPrzygoda
                                select gp).Count();

            return liczbaGraczy;
        }

        private void DodajGraczPrzygoda(int idGracz, Przygoda przygoda)
        {
            db.GraczPrzygody.Add(new GraczPrzygoda { IdGracz = idGracz, IdPrzygoda = przygoda.IdPrzygoda});
            db.SaveChanges();
        }


        private void ZmianaStatusuPrzygody(int liczbaGraczy, Przygoda przygoda)
        {
            przygoda.Status = Status.Rozpoczeta;
            db.Entry(przygoda).State = EntityState.Modified;
            db.SaveChanges();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
