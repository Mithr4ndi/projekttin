﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AplikacjaDnD.Models;

namespace AplikacjaDnD.Controllers
{
    public class GraczPrzygodaController : Controller
    {
        private DbDnD db = new DbDnD();

        // GET: GraczPrzygoda
        public ActionResult Index()
        {
            var graczPrzygody = db.GraczPrzygody.Include(g => g.Gracz).Include(g => g.Przygoda);
            return View(graczPrzygody.ToList());
        }

        public ActionResult Index(string sortOrder)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            var gP = from s in db.GraczPrzygody
                         select s;
            switch (sortOrder)
            {
                case "name_desc":
                    gP = gP.OrderByDescending(g => g.DataRozpoczecia);
                    break;
                default:
                    gP = gP.OrderBy(g => g.DataRozpoczecia);
                    break;
            }



            return View(gP.ToList());
        }

        // GET: GraczPrzygoda/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GraczPrzygoda graczPrzygoda = db.GraczPrzygody.Include(g => g.Przygoda).Where(g => g.IdGraczPrzygoda == id).First();
            if (graczPrzygoda == null)
            {
                return HttpNotFound();
            }
            return View(graczPrzygoda);
        }

        // GET: GraczPrzygoda/Create
        public ActionResult Create()
        {
            ViewBag.IdGracz = new SelectList(db.Osoby, "IdOsoba", "Imie");
            ViewBag.IdPrzygoda = new SelectList(db.Przygody, "IdPrzygoda", "Tytul");
            return View();
        }

        // POST: GraczPrzygoda/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdGraczPrzygoda,IdGracz,IdPrzygoda,DataRozpoczecia,DataZakoczenia")] GraczPrzygoda graczPrzygoda)
        {
            if (ModelState.IsValid)
            {
                db.GraczPrzygody.Add(graczPrzygoda);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdGracz = new SelectList(db.Osoby, "IdOsoba", "Imie", graczPrzygoda.IdGracz);
            ViewBag.IdPrzygoda = new SelectList(db.Przygody, "IdPrzygoda", "Tytul", graczPrzygoda.IdPrzygoda);
            return View(graczPrzygoda);
        }

        // GET: GraczPrzygoda/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GraczPrzygoda graczPrzygoda = db.GraczPrzygody.Find(id);
            if (graczPrzygoda == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdGracz = new SelectList(db.Osoby, "IdOsoba", "Imie", graczPrzygoda.IdGracz);
            ViewBag.IdPrzygoda = new SelectList(db.Przygody, "IdPrzygoda", "Tytul", graczPrzygoda.IdPrzygoda);
            return View(graczPrzygoda);
        }

        // POST: GraczPrzygoda/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdGraczPrzygoda,IdGracz,IdPrzygoda,DataRozpoczecia,DataZakoczenia")] GraczPrzygoda graczPrzygoda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(graczPrzygoda).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdGracz = new SelectList(db.Osoby, "IdOsoba", "Imie", graczPrzygoda.IdGracz);
            ViewBag.IdPrzygoda = new SelectList(db.Przygody, "IdPrzygoda", "Tytul", graczPrzygoda.IdPrzygoda);
            return View(graczPrzygoda);
        }

        // GET: GraczPrzygoda/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GraczPrzygoda graczPrzygoda = db.GraczPrzygody.Find(id);
            if (graczPrzygoda == null)
            {
                return HttpNotFound();
            }
            return View(graczPrzygoda);
        }

        // POST: GraczPrzygoda/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GraczPrzygoda graczPrzygoda = db.GraczPrzygody.Find(id);
            db.GraczPrzygody.Remove(graczPrzygoda);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult RozpoczetePrzygody()
        {
            var idGracz = (int)Session["UserId"];

            var graczPrzygody = db.GraczPrzygody.Include(g => g.Gracz).Include(g => g.Przygoda).Where(g=>g.Przygoda.Status==Status.Rozpoczeta && g.IdGracz==idGracz);

  
            List<RozpoczetePrzygodyModelView> rozpoczetView = new List<RozpoczetePrzygodyModelView>();

            foreach (var item in graczPrzygody)
            {
                rozpoczetView.Add(new RozpoczetePrzygodyModelView
                {
                    IdGraczPrzygoda = item.IdGraczPrzygoda,
                    Nick = item.Przygoda.MistrzGry.Nick,
                    Tytul = item.Przygoda.Tytul,
                    DataRozpoczecia = (DateTime)item.DataRozpoczecia,
                    PoziomTrudnosci = item.Przygoda.PoziomTrudnosci,
                    Fabula = item.Przygoda.Fabula,
                    LiczbaLokacji = item.Przygoda.LiczbaLokacji,
                    LiczbaGraczy = item.Przygoda.LiczbaGraczy
                });
            }
            return View(rozpoczetView);
        }

        public ActionResult ZakonczonePrzygody()
        {
            var idGracz = (int)Session["UserId"];

            var graczPrzygody = db.GraczPrzygody.Include(g => g.Gracz).Include(g => g.Przygoda)
                .Where(g => g.Przygoda.Status == Status.Zakonczona && g.IdGracz == idGracz);

            List<ZakonczonePrzygodyModelView> zakonczoneView = new List<ZakonczonePrzygodyModelView>();

            foreach (var item in graczPrzygody)
            {
                zakonczoneView.Add(new ZakonczonePrzygodyModelView
                {
                    IdGraczPrzygoda = item.IdGraczPrzygoda,
                    Nick = item.Przygoda.MistrzGry.Nick,
                    Tytul = item.Przygoda.Tytul,
                    DataRozpoczecia = (DateTime)item.DataRozpoczecia,
                    DataZakonczenia = (DateTime)item.DataZakonczenia,
                    PoziomTrudnosci = item.Przygoda.PoziomTrudnosci,
                    Fabula = item.Przygoda.Fabula,
                    LiczbaLokacji = item.Przygoda.LiczbaLokacji,
                    LiczbaGraczy = item.Przygoda.LiczbaGraczy
                });
            }
            return View(zakonczoneView);
        }


        public ActionResult RozpoczetePrzygodyKontynuuj()
        {
            var idGracz = (int)Session["UserId"];

            var graczPrzygody = db.GraczPrzygody.Include(g => g.Gracz).Include(g => g.Przygoda).Where(g => g.Przygoda.Status == Status.Rozpoczeta && g.IdGracz == idGracz);


            List<RozpoczetePrzygodyModelView> rozpoczetView = new List<RozpoczetePrzygodyModelView>();

            foreach (var item in graczPrzygody)
            {
                rozpoczetView.Add(new RozpoczetePrzygodyModelView
                {
                    IdGraczPrzygoda = item.IdGraczPrzygoda,
                    Nick = item.Przygoda.MistrzGry.Nick,
                    Tytul = item.Przygoda.Tytul,
                    DataRozpoczecia = (DateTime)item.DataRozpoczecia,
                    PoziomTrudnosci = item.Przygoda.PoziomTrudnosci,
                    Fabula = item.Przygoda.Fabula,
                    LiczbaLokacji = item.Przygoda.LiczbaLokacji,
                    LiczbaGraczy = item.Przygoda.LiczbaGraczy
                });
            }
            return View(rozpoczetView);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
