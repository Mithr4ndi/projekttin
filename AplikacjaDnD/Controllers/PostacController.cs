﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AplikacjaDnD.Models;

namespace AplikacjaDnD.Controllers
{
    public class PostacController : Controller
    {
        private DbDnD db = new DbDnD();

        // GET: Postac
        public ActionResult Index()
        {
            var idGracz = (int)Session["UserId"];
           // var postaci = db.Postaci.Include(p => p.Gracz).Include(p => p.Klasa).Include(p => p.Rasa);

            var result = from p in db.Postaci
                         where p.IdGracz == idGracz
                         select p;

            return View(result.ToList());
        }

        // GET: Postac/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postac postac = db.Postaci.Find(id);
            if (postac == null)
            {
                return HttpNotFound();
            }
            return View(postac);
        }

        // GET: Postac/Create
        public ActionResult Create()
        {
            ViewBag.IdGracz = new SelectList(db.Osoby, "IdOsoba", "Nick");
            ViewBag.IdKlasa = new SelectList(db.Klasy, "IdKlasa", "Nazwa");
            ViewBag.IdRasa = new SelectList(db.Rasy, "IdRasa", "Nazwa");
         
            return View();
        }

        // POST: Postac/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPostac,IdKlasa,IdRasa,Nazwa,Poziom,PoziomZdrowia,PoziomMany,Doswiadczenie")] Postac postac)
        {
            if (ModelState.IsValid)
            {
                postac.IdGracz = (int)Session["UserId"];
                db.Postaci.Add(postac);
                db.SaveChanges();
                return View("PostacUtworzona");
            }

            ViewBag.IdGracz = new SelectList(db.Osoby, "IdOsoba", "Nick", postac.IdGracz);
            ViewBag.IdKlasa = new SelectList(db.Klasy, "IdKlasa", "Nazwa", postac.IdKlasa);
            ViewBag.IdRasa = new SelectList(db.Rasy, "IdRasa", "Nazwa", postac.IdRasa);
            return View(postac);
        }

        // GET: Postac/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postac postac = db.Postaci.Find(id);
            if (postac == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdGracz = new SelectList(db.Osoby, "IdOsoba", "Nick", postac.IdGracz);
            ViewBag.IdKlasa = new SelectList(db.Klasy, "IdKlasa", "Nazwa", postac.IdKlasa);
            ViewBag.IdRasa = new SelectList(db.Rasy, "IdRasa", "Nazwa", postac.IdRasa);
            return View(postac);
        }

        // POST: Postac/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPostac,IdGracz,IdKlasa,IdRasa,Nazwa,Poziom,PoziomZdrowia,PoziomMany,Doswiadczenie")] Postac postac)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postac).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdGracz = new SelectList(db.Osoby, "IdOsoba", "Nick", postac.IdGracz);
            ViewBag.IdKlasa = new SelectList(db.Klasy, "IdKlasa", "Nazwa", postac.IdKlasa);
            ViewBag.IdRasa = new SelectList(db.Rasy, "IdRasa", "Nazwa", postac.IdRasa);
            return View(postac);
        }

        // GET: Postac/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postac postac = db.Postaci.Find(id);
            if (postac == null)
            {
                return HttpNotFound();
            }
            return View(postac);
        }

        // POST: Postac/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Postac postac = db.Postaci.Find(id);
            db.Postaci.Remove(postac);
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        public ActionResult WybierzPostac()
        {
            var idGracz = (int)Session["UserId"];
            var result = (from p in db.Postaci
                          where p.IdGracz == idGracz
                          select p).Count();

            if (result == 0)
            {
                return View("BrakPostaci");
            }

            var postaci = db.Postaci.Include(p => p.Gracz).Include(p => p.Klasa).Include(p => p.Rasa).Where(p => p.IdGracz == idGracz);  
            return View(postaci.ToList());
        }

        public ActionResult WybierzPostacKontynuuj()
        {
            var idGracz = (int)Session["UserId"];
            var result = (from p in db.Postaci
                          where p.IdGracz == idGracz
                          select p).Count();

            if (result == 0)
            {
                return View("BrakPostaci");
            }

            var postaci = db.Postaci.Include(p => p.Gracz).Include(p => p.Klasa).Include(p => p.Rasa).Where(p => p.IdGracz == idGracz);

            return View(postaci.ToList());
        }

        public ActionResult Wybierz(int? id)
        {
            if (id == null)
            {
                return View("WybierzPostac");
            }

            return View("DolaczDoPrzygody", "Przygoda");
        }

        public JsonResult IsNazwaExist(string Nazwa, int? Id)
        {
            var validateName = db.Postaci.FirstOrDefault
                                (x => x.Nazwa == Nazwa && x.IdPostac != Id);
            if (validateName != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
