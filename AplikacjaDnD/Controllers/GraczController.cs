﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AplikacjaDnD.Models;
using AplikacjaDnD.Models.ViewModels;

namespace AplikacjaDnD.Controllers
{
    public class GraczController : Controller
    {
        private DbDnD db = new DbDnD();

        // GET: Gracz
        public ActionResult Index()
        {
            int session = (int)Session["UserId"];
            Osoba g = db.Osoby.Where(o => o.IdOsoba == session).First();
            return View(g);
        }

        // GET: Gracz/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gracz gracz = db.Gracze.Include("Postaci").Where(p => p.IdOsoba == id).First();
            if (gracz == null)
            {
                return HttpNotFound();
            }
            return View(gracz);
        }

        // GET: Gracz/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Gracz/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdOsoba,Imie,Nazwisko,Nick,Haslo,Email,DataUrodzenia")] Gracz gracz)
        {
            if (ModelState.IsValid)
            {
                db.Osoby.Add(gracz);
                db.SaveChanges();
                return View("GraczUtworzony");
            }

            return View(gracz);
        }

        // GET: Gracz/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gracz gracz = (Gracz)db.Osoby.Find(id);
            if (gracz == null)
            {
                return HttpNotFound();
            }
            return View(gracz);
        }

        // POST: Gracz/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdOsoba,Imie,Nazwisko,Nick,Email,DataUrodzenia")] Gracz gracz)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gracz).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gracz);
        }

        // GET: Gracz/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gracz gracz = (Gracz)db.Osoby.Find(id);
            if (gracz == null)
            {
                return HttpNotFound();
            }
            return View(gracz);
        }

        // POST: Gracz/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Gracz gracz = (Gracz)db.Osoby.Find(id);
            db.Osoby.Remove(gracz);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Gracz/Start
        public ActionResult Start()
        {

            return View();
        }

        // POST: Gracz/Start
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Start([Bind(Include = "Nick,Haslo")] StratViewModel start)
        {
            var dbResult = db.Gracze.Where(g => g.Nick.Equals(start.Nick)&&g.Haslo.Equals(start.Haslo));
            

            if (dbResult.Count() > 0)
            {
                var result = dbResult.First();
                Session["UserNick"] = result.Nick;
                Session["UserId"] = result.IdOsoba;
                return View("Menu");
            }

            ModelState.AddModelError("", "Błędny NICK lub HASŁO.");

            return View();
        }

        // POST: Gracz/Start
        
        [HttpPost]
        [ValidateAntiForgeryToken]
       public ActionResult StartDynamic([Bind(Include = "Nick, isMistrzGry")] StratViewModel start)
        {
            var dbResult = db.Osoby.Where(g => g.Nick.Equals(start.Nick));

            if (dbResult.Count() > 0)
            {
                //Tu powinna byc refaktoryzacja, ale zostalo tu dla ulatwienia prezentacji :)
                if (!start.isMistrzGry) {
                    var graczResult = db.Gracze.Where(g => g.Nick.Equals(start.Nick));
                    var result = graczResult.First();
                    result.IsMistrzGry = false;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    Session["UserNick"] = result.Nick;
                    Session["UserId"] = result.IdOsoba;
                    return View("Menu");
                } else
                {
                    var graczResult = db.MistrzowieGry.Where(g => g.Nick.Equals(start.Nick));
                    var result = graczResult.First();
                    result.IsMistrzGry = true;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    Session["UserNick"] = result.Nick;
                    Session["UserId"] = result.IdOsoba;
                    return View("MenuMistrzGry");
                }
            }

            return View();
        }
       

        public JsonResult IsNickExist(string Nick, int? IdOsoba)
        {
            var validateName = db.Gracze.FirstOrDefault
                                (x => x.Nick == Nick && x.IdOsoba != IdOsoba);
            if (validateName != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult IsEmailExist(string Email, int? IdOsoba)
        {
            var validateName = db.Gracze.FirstOrDefault
                                (x => x.Email == Email && x.IdOsoba != IdOsoba);
            if (validateName != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
        //GET: Gracz/Menu
        public ActionResult Menu()
        {

            return View();
        }

    
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
