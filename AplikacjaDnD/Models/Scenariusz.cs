﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    [Table("Scenariusze")]
    public class Scenariusz
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdScenariusz { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(255)]
        public string Tytul { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(500)]
        public string Tresc { get; set; }
        [ForeignKey("MistrzGry")]
        public int IdMistrzGry { get; set; }

        public virtual MistrzGry MistrzGry { get; set; }
    }
}