﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models.ViewModels
{
    public class StratViewModel
    {
        [Required(ErrorMessage = "Podaj nick.")]
        public string Nick { get; set; }
        [Required(ErrorMessage = "Podaj hasło.")]
        public string Haslo { get; set; }

        public bool isMistrzGry { get; set; }
    }
}