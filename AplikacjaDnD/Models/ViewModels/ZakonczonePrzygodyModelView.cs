﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    public class ZakonczonePrzygodyModelView
    {
        public int IdGraczPrzygoda { get; set; }
        public string Nick { get; set; }
        public string Tytul { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DataRozpoczecia { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DataZakonczenia { get; set; }
        public PoziomTrudnosci PoziomTrudnosci { get; set; }
        public string Fabula { get; set; }
        public int LiczbaLokacji { get; set; }
        public int LiczbaGraczy { get; set; }
    }
}