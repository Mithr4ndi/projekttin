﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace AplikacjaDnD.Models.ViewModels
{
    public class PostacRasaViewModel
    {
        [Required(ErrorMessage = "Podaj nazwę.")]
        [Index(IsUnique = true)]
        public string NazwaPostaci { get; set; }

        [Required(ErrorMessage = "To pole jest wymagane.")]
        public string Plec { get; set; }

        [Required(ErrorMessage = "To pole jest wymagane.")]
        public string Rasa { get; set; }
    }
}