﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    public class DbDnD : DbContext
    {
        public DbSet<Osoba> Osoby { get; set; }
        public DbSet<Gracz> Gracze { get; set; }
        public DbSet<MistrzGry> MistrzowieGry { get; set; }
        public DbSet<Postac> Postaci { get; set; }
        public DbSet<Przygoda> Przygody { get; set; }
        public DbSet<Potwor> Potwory { get; set; }
        public DbSet<Lokacja> Lokacje { get; set; }
        public DbSet<Zadanie> Zadania { get; set; }
        public DbSet<Klasa> Klasy { get; set; }
        public DbSet<Rasa> Rasy { get; set; }
        public DbSet<Pancerz> Pancerze { get; set; }
        public DbSet<Bron> Bronie { get; set; }
        public DbSet<GraczPrzygoda> GraczPrzygody { get; set; }
        public DbSet<Scenariusz> Scenariusze { get; set; }

    }
}