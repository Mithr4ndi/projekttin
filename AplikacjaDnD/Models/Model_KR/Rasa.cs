﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    [Table("Rasy")]
    public class Rasa : ModelKlasaRasa
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdRasa { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public string Przywilej { get; set; }
        public virtual ICollection<Postac> Postaci { get; set; }
    }
}