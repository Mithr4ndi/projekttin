﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    [Table("Klasy")]
    public class Klasa : ModelKlasaRasa
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKlasa { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public string BonusKlasowy { get; set; }
        [MaxLength(10)]
        public virtual ICollection<Umiejetnosc> Umiejetnosci { get; set; }
        public virtual ICollection<Postac> Postaci { get; set; }

    }
}