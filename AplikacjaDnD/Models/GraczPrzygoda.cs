﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    [Table("GraczPrzygody")]
    public class GraczPrzygoda
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdGraczPrzygoda { get; set; }
        [ForeignKey("Gracz")]
        [Index("IX_FirstAndSecond", 1, IsUnique = true)]
        public int IdGracz { get; set; }
        
        [ForeignKey("Przygoda")]
        [Index("IX_FirstAndSecond", 2, IsUnique = true)]
        public int IdPrzygoda { get; set; }
       
        private DateTime dataRozpoczecia;

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DataRozpoczecia
        {
            get
            {
                return dataRozpoczecia;
            }
            set
            {
                dataRozpoczecia = DateTime.Now;
            }
        }

        public DateTime? DataZakonczenia { get; set; }

        
        public virtual Gracz Gracz { get; set; }
        public virtual Przygoda Przygoda { get; set; }
    }
}