﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    public enum RodzajPancerza
    {
        Helm,
        Naramienniki,
        Karwasze,
        Rekawice,
        Napiersnik,
        Nagolenniki,
        Peleryna,
        Buty,
        Tarcza
    }

    public enum TypPancerza
    {
        Plocienny,
        Skorzany,
        Kolczy,
        Plytowy
    }

    [Table("Pancerz")]
    public class Pancerz : Przedmiot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPancerz { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public RodzajBroni Rodzaj { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public TypPancerza Typ { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public int PunktyPancerza { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public int Wytrzymalosc { get; set; }
        [ForeignKey("Postac")]
        public int IdPostac { get; set; }
        public virtual Postac Postac { get; set; }

    }
}