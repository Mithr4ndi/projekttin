﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    public enum RodzajBroni
    {
        Miecz,
        Luk,
        Topor,
        Wlocznia,
        Kusza,
        Sztylet,
        Laska
    }

    [Table("Bronie")]
    public class Bron : Przedmiot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdBron { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public RodzajBroni Rodzaj { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public int Obrazenia { get; set; }
        public int? Zasieg { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public int Predkosc { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(100)]
        public string Bonus { get; set; }

        [ForeignKey("Postac")]
        public int IdPostac { get; set; }
        public virtual Postac Postac { get; set; }

    }
}