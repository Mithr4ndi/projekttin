﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
   public enum Rzadkosc
    {
        Pospolity,
        Rzadki,
        Bardzo_Rzadki,
        Legendarny
    }
    
    public abstract class Przedmiot  : ModelPrzedmiotUmiejetnoscLokacjaZadanie
    {
        [Required(ErrorMessage = "To pole jest wymagane")]
        public Rzadkosc Rzadkosc { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public bool czyMagiczny { get; set; }
    }
}