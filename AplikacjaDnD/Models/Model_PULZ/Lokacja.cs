﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    [Table("Lokacje")]
    public class Lokacja : ModelPrzedmiotUmiejetnoscLokacjaZadanie
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdLokacja { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public string Krolestwo { get; set; }
        [ForeignKey("Przygoda")]
        public int IdPrzygoda { get; set; }

        public virtual Przygoda Przygoda { get; set; }

        public virtual ICollection<Zadanie> Zadania { get; private set; }
        public virtual ICollection<Potwor> Potwory {get; private set; }


    }
}