﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    public enum Skutek
    {
        Obrazenia,
        Leczenie
    }

    [Table("Umiejetnosci")]
    public class Umiejetnosc : ModelPrzedmiotUmiejetnoscLokacjaZadanie
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdUmiejetnosc { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public int Moc { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public Skutek Efekt { get; set; }
        [ForeignKey("Klasa")]
        public int IdKlasa { get; set; }
        public virtual Klasa Klasa { get; set; }


    }
}