﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    public abstract class ModelPrzedmiotUmiejetnoscLokacjaZadanie
    {
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(20)]
        [Index(IsUnique = true)]
        public string Nazwa { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public int Poziom { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public string Opis { get; set; }
    }
}