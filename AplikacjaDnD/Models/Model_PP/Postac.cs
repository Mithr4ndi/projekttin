﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplikacjaDnD.Models
{
    [Table("Postaci")]
    public class Postac : ModelPostacPotwor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPostac { get; set; }
        [Remote("IsNazwaExist", "Postac", AdditionalFields = "IdPostac",
        ErrorMessage = "Podany nazwa jest zajęta")]
        public override string Nazwa { get; set; }
        [ForeignKey("Gracz")]
        public int IdGracz { get; set; }
        [ForeignKey("Klasa")]
        public int IdKlasa { get; set; }
        [ForeignKey("Rasa")]
        public int IdRasa { get; set; }

        public override int? PoziomZdrowia { get; set; } = 150;
        public override int? Poziom { get; set; } = 1;
        public override int? Doswiadczenie { get; set; } = 0;
        public virtual Rasa Rasa { get; set; }
        public virtual Klasa Klasa { get; set; }
        public virtual Gracz Gracz { get; private set; }
        public virtual ICollection<Bron> Bronie { get; set; }
        public virtual ICollection<Pancerz> Pancerze { get; set; }

        public Postac(){}

        public void dodajPostac(Postac postac) { }


    }
}