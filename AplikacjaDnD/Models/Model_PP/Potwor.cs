﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    [Table("Potwory")]
    public class Potwor : ModelPostacPotwor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPotwor { get; set; }
        public override string Nazwa { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public string Typ { get; set; }    
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(500)]
        public string Opis { get; set; }
        public virtual Lokacja Lokacja { get; set; }
        public override int? Poziom { get; set; } = 10;
        public override int? PoziomZdrowia { get; set; } = 200;
        public override int? Doswiadczenie { get; set; } = 50;
    }
}