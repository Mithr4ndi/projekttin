﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplikacjaDnD.Models
{
    public abstract class ModelPostacPotwor
    {
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(20)]
        [Index(IsUnique = true)]
        public abstract string Nazwa { get; set; }
        abstract  public int? Poziom { get; set; }
        abstract public int? PoziomZdrowia { get; set; }
        public int? PoziomMany { get;  private set; }
        abstract public int? Doswiadczenie { get; set; }
    }
}