﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    [Table("Zadania")]
    public class Zadanie : ModelPrzedmiotUmiejetnoscLokacjaZadanie
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdZadanie { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public string Nagroda { get; set; }
        [ForeignKey("Lokacja")]
        public int IdLokacja { get; set; }

        public virtual Lokacja Lokacja { get; private set; }

       
    }
}