﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    public class MistrzGry : Osoba
    {
        public virtual ICollection<Przygoda> Przygody { get; set; }
        public virtual ICollection<Scenariusz> Scenariusze { get; set; }
    }
}