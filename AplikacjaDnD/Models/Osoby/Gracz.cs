﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    public class Gracz : Osoba
    {
        [MaxLength(10)]
        public virtual ICollection<Postac> Postaci { get; private set; }

        public virtual ICollection<GraczPrzygoda> GraczPrzygody { get; set; }

        public static Postac UtworzPostac(Gracz graczCalosc, String nazwa, Klasa klasaPostaci, Rasa rasa) { return null;  }
    }
}