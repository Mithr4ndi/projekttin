﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplikacjaDnD.Models
{
    [Table("Osoby")]
    public abstract class Osoba
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdOsoba { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(100)]
        public string Imie { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(100)]
        public string Nazwisko { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(20)]
        [Index(IsUnique = true)]
        [Remote("IsNickExist", "Gracz", AdditionalFields = "IdOsoba",
        ErrorMessage = "Podany nick jest zajęty")]
        public string Nick { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(100)]
        [Index(IsUnique = true)]
        [Remote("IsEmailExist", "Gracz", AdditionalFields = "IdOsoba",
        ErrorMessage = "Podany email już istnieje w bazie")]
        [RegularExpression((@"^[0-9a-zA-Z_.-]+@[0-9a-zA-Z.-]+\.[a-zA-Z]{2,3}$"), ErrorMessage = "Niepoprawny format email!")]
        public string Email { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = false)]
        public DateTime DataUrodzenia { get; set; }

        [Required(ErrorMessage = "To pole jest wymagane")]
        [StringLength(18, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$", ErrorMessage = "Hasło musi posiadać minimum 6 znaków, jedną dużą literę i znak specjalny.")]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Haslo { get; set; }
        public bool IsMistrzGry { get; set; } = false;
    }
}