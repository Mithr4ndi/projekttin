﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplikacjaDnD.Models
{
    public enum PoziomTrudnosci
    {
        Latwy,
        Normalny,
        Trudny
    }

    public enum Status
    {
        Rozpoczeta,
        Zakonczona,
        Brak
    }

    [Table("Przygody")]
    public class Przygoda
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPrzygoda { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [MaxLength(30)]
        public string Tytul { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        public PoziomTrudnosci PoziomTrudnosci { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [Range(3,15, ErrorMessage = "W przygodzie moze brac udzial od 3 do 15 graczy")]
        public int LiczbaGraczy { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane")]
        [Range(2,10, ErrorMessage = "Liczba lokacji moze wynosic od 2 do 10")]
        public int LiczbaLokacji { get; set; }
        [MaxLength(500)]
        public string Fabula { get; set; }
        [ForeignKey("MistrzGry")]
        public int IdMistrzGry { get; set; }

        public Status Status { get; set; } = Status.Brak;

        public virtual MistrzGry MistrzGry { get; set; }

        public virtual ICollection<Lokacja> Lokacje { get; set; }
        public virtual ICollection<GraczPrzygoda> GraczPrzygody { get; set; }

    }
}